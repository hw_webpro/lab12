import { IsNotEmpty, Length } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(5, 30)
  name: string;

  @IsNotEmpty()
  age: number;

  @IsNotEmpty()
  @Length(10, 10)
  tel: string;

  @IsNotEmpty()
  @Length(1, 1)
  gender: string;
}
